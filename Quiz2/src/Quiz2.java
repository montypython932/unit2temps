import java.util.Scanner;

public class Quiz2 {
	
	static String username = "Phil";
	static String password = "12345";
	static String nameInput,
		   passInput;
	
	public static void main(String[] args) {
		if (authenticateUsername(getUsername())) {
			if (authenticatePassword(getPassword())) {
				System.out.println("Welcome, " + username);
			} else {
				System.out.println("Invalid Password!");
			}
		} else {
			System.out.println("Invalid Username");
		}
	}
	
	//Authenticates the username
	public static boolean authenticateUsername(String name) {
		if (name.equals(username)) {
			return true;
		} else {
			return false;
		}
	}
	
	//Authenticates the password
	public static boolean authenticatePassword(String pass) {
		if (pass.equals(password)) {
			return true;
		} else {
			return false;
		}
	}
	
	//Gets username as input from user
	public static String getUsername() {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter your username: ");
		nameInput = input.next();
		return nameInput;
	}
	
	//Gets password as input from user
	public static String getPassword() {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter your password: ");
		passInput = input.next();
		return passInput;
	}
}
